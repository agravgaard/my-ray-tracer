#ifndef HITTABLE_LIST_HPP
#define HITTABLE_LIST_HPP

#include "hittable.hpp"

#include <memory>
#include <vector>

template <class T>
requires std::floating_point<T> class hittable_list : public hittable<T> {
public:
  constexpr hittable_list() = default;
  explicit constexpr hittable_list(std::shared_ptr<hittable<T>> object) {
    add(object);
  }

  constexpr void clear() { objects.clear(); }
  constexpr void add(std::shared_ptr<hittable<T>> object) {
    objects.push_back(object);
  }

  constexpr auto hit(const ray<T> &r, T t_min, T t_max,
                     hit_record<T> &rec) const -> bool override;

  constexpr auto bounding_box(T time0, T time1, aabb<T> &output_box) const
      -> bool override;

  [[nodiscard]] auto pdf_value(const point3<T> &o, const vec3<T> &v) const
      -> T override {
    const auto weight = 1.0 / objects.size();
    auto sum = 0.0;

    for (const auto &object : objects) {
      sum += weight * object->pdf_value(o, v);
    }

    return sum;
  }

  [[nodiscard]] auto random(const vec3<T> &o) const -> vec3<T> override {
    auto int_size = objects.size();
    return objects[static_cast<size_t>(random_between<T>(0, int_size))]->random(
        o);
  }

  // public:
  std::vector<std::shared_ptr<hittable<T>>> objects;
};

template <class T>
requires std::floating_point<T> constexpr auto
hittable_list<T>::hit(const ray<T> &r, T t_min, T t_max,
                      hit_record<T> &rec) const -> bool {
  hit_record<T> temp_rec;
  bool hit_anything = false;
  auto closest_so_far = t_max;

  for (const auto &object : objects) {
    if (object->hit(r, t_min, closest_so_far, temp_rec)) {
      hit_anything = true;
      closest_so_far = temp_rec.t;
      rec = temp_rec;
    }
  }

  return hit_anything;
}

template <class T>
requires std::floating_point<T> constexpr auto
hittable_list<T>::bounding_box(T time0, T time1, aabb<T> &output_box) const
    -> bool {
  if (objects.empty()) {
    return false;
  }

  aabb<T> temp_box;
  bool first_box = true;

  for (const auto &object : objects) {
    if (!object->bounding_box(time0, time1, temp_box)) {
      return false;
    }
    output_box = first_box ? temp_box : surrounding_box(output_box, temp_box);
    first_box = false;
  }

  return true;
}

#endif
