#ifndef BOX_HPP
#define BOX_HPP

#include <concepts>

#include "aarect.hpp"
#include "hittable_list.hpp"

template <class T>
requires std::floating_point<T> class box : public hittable<T> {
public:
  box() = default;
  box(const point3<T> &p0, const point3<T> &p1,
      std::shared_ptr<material<T>> ptr);

  auto hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
      -> bool override {
    return sides.hit(r, t_min, t_max, rec);
  }

  auto bounding_box(T /*time0*/, T /*time1*/, aabb<T> &output_box) const
      -> bool override {
    output_box = aabb(box_min, box_max);
    return true;
  }

private:
  point3<T> box_min;
  point3<T> box_max;
  hittable_list<T> sides;
};

template <class T>
requires std::floating_point<T> box<T>::box(const point3<T> &p0,
                                            const point3<T> &p1,
                                            std::shared_ptr<material<T>> ptr) {
  box_min = p0;
  box_max = p1;

  sides.add(std::make_shared<xy_rect<T>>(p0.x(), p1.x(), p0.y(), p1.y(), p1.z(),
                                         ptr));
  sides.add(std::make_shared<xy_rect<T>>(p0.x(), p1.x(), p0.y(), p1.y(), p0.z(),
                                         ptr));

  sides.add(std::make_shared<xz_rect<T>>(p0.x(), p1.x(), p0.z(), p1.z(), p1.y(),
                                         ptr));
  sides.add(std::make_shared<xz_rect<T>>(p0.x(), p1.x(), p0.z(), p1.z(), p0.y(),
                                         ptr));

  sides.add(std::make_shared<yz_rect<T>>(p0.y(), p1.y(), p0.z(), p1.z(), p1.x(),
                                         ptr));
  sides.add(std::make_shared<yz_rect<T>>(p0.y(), p1.y(), p0.z(), p1.z(), p0.x(),
                                         ptr));
}

#endif
