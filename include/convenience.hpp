#ifndef CONVENIENCE_HPP
#define CONVENIENCE_HPP

#include <array>
#include <concepts>
#include <limits>
#include <numbers>
#include <random>
#include <type_traits>

template <typename T, size_t N>
using array3d = std::array<std::array<std::array<T, N>, N>, N>;

// constants
constexpr auto thousand = 1000;
constexpr auto thirteen = 13;
constexpr auto twelve = 12;
constexpr auto ten = 10;
constexpr auto five = 5;
constexpr auto p95 = 0.95;
constexpr auto p9 = 0.9;
constexpr auto p8 = 0.8;
constexpr auto p7 = 0.7;
constexpr auto p6 = 0.6;
constexpr auto half = 0.5;
constexpr auto p4 = 0.4;
constexpr auto p3 = 0.3;
constexpr auto p2 = 0.2;
constexpr auto p1 = 0.1;

template <class T>
requires std::floating_point<T> constexpr auto pi = std::numbers::pi_v<T>;

template <class T>
requires std::floating_point<T> constexpr auto
    infinity = std::numeric_limits<T>::infinity();

constexpr auto halfc_deg = 180.0;

// Utility Functions

template <class T>
requires std::floating_point<T> constexpr auto degrees_to_radians(T degrees)
    -> T {
  return degrees * pi<T> / static_cast<T>(halfc_deg);
}

template <typename T> auto get_engine() -> std::default_random_engine & {
  // Initialized upon first call to the function.
  thread_local static std::default_random_engine engine(
      static_cast<unsigned int>(time(nullptr)));
  return engine;
}

template <typename T, T low, T high>
requires std::integral<T> auto get_random()
    -> std::uniform_int_distribution<T> & {
  // Initialized upon first call to the function.
  thread_local static std::uniform_int_distribution<T> random_int(low, high);
  return random_int;
}

template <typename T>
requires std::floating_point<T> auto get_random()
    -> std::uniform_real_distribution<T> & {
  // Initialized upon first call to the function.
  thread_local static std::uniform_real_distribution<T> random_real(0.0, 1.0);
  return random_real;
}

template <typename T>
requires std::floating_point<T> auto random_between(T low, T high) -> T {
  auto r = get_random<T>()(get_engine<T>());
  return low + (r * (high - low));
}

template <typename T, T low, T high>
requires std::integral<T> auto random_between() -> T {
  return get_random<T, low, high>()(get_engine<T>());
}

template <typename T> requires std::floating_point<T> auto random_real() -> T {
  return get_random<T>()(get_engine<T>());
}

#endif
