#ifndef BVH_HPP
#define BVH_HPP

#include <algorithm>
#include <memory>
#include <vector>

#include "convenience.hpp"
#include "hittable.hpp"
#include "hittable_list.hpp"

template <class T>
requires std::floating_point<T> class bvh_node : public hittable<T> {
public:
  bvh_node();

  bvh_node(const hittable_list<T> &list, T time0, T time1)
      : bvh_node(list.objects, 0, list.objects.size(), time0, time1) {}

  bvh_node(const std::vector<std::shared_ptr<hittable<T>>> &src_objects,
           size_t start, size_t end, T time0, T time1);

  auto hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
      -> bool override;

  constexpr auto bounding_box(T time0, T time1, aabb<T> &output_box) const
      -> bool override;

  std::shared_ptr<hittable<T>> left;
  std::shared_ptr<hittable<T>> right;
  aabb<T> box;
};

template <class T>
requires std::floating_point<T> bvh_node<T>::bvh_node(
    const std::vector<std::shared_ptr<hittable<T>>> &src_objects, size_t start,
    size_t end, T time0, T time1) {
  // Create a modifiable array of the source scene objects
  auto objects = src_objects;

  auto axis = random_between<int, 0, 2>();
  auto comparator = [axis](auto a, auto b) {
    return (axis == 0)   ? box_x_compare<T>(a, b)
           : (axis == 1) ? box_y_compare<T>(a, b)
                         : box_z_compare<T>(a, b);
  };

  size_t object_span = end - start;

  if (object_span == 1) {
    left = right = objects[start];
  } else if (object_span == 2) {
    if (comparator(objects[start], objects[start + 1])) {
      left = objects[start];
      right = objects[start + 1];
    } else {
      left = objects[start + 1];
      right = objects[start];
    }
  } else {
    std::sort(objects.begin() + start, objects.begin() + end, comparator);

    auto mid = start + object_span / 2;
    left = std::make_shared<bvh_node<T>>(objects, start, mid, time0, time1);
    right = std::make_shared<bvh_node<T>>(objects, mid, end, time0, time1);
  }

  aabb<T> box_left;
  aabb<T> box_right;

  if (!left->bounding_box(time0, time1, box_left) ||
      !right->bounding_box(time0, time1, box_right)) {
    std::cerr << "No bounding box in bvh_node constructor.\n";
  }

  box = surrounding_box(box_left, box_right);
}

template <class T>
requires std::floating_point<T> auto
bvh_node<T>::hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
    -> bool {
  if (!box.hit(r, t_min, t_max)) {
    return false;
  }

  bool hit_left = left->hit(r, t_min, t_max, rec);
  bool hit_right = right->hit(r, t_min, hit_left ? rec.t : t_max, rec);

  return hit_left || hit_right;
}

template <class T>
requires std::floating_point<T> constexpr auto
bvh_node<T>::bounding_box(T /*time0*/, T /*time1*/, aabb<T> &output_box) const
    -> bool {
  output_box = box;
  return true;
}

template <class T>
requires std::floating_point<T> constexpr auto
box_compare(const std::shared_ptr<hittable<T>> a,
            const std::shared_ptr<hittable<T>> b, int axis) -> bool {
  aabb<T> box_a;
  aabb<T> box_b;

  if (!a->bounding_box(0, 0, box_a) || !b->bounding_box(0, 0, box_b)) {
    std::cerr << "No bounding box in bvh_node constructor.\n";
  }

  return box_a.min()[axis] < box_b.min()[axis];
}

template <class T>
requires std::floating_point<T> constexpr auto
box_x_compare(const std::shared_ptr<hittable<T>> a,
              const std::shared_ptr<hittable<T>> b) -> bool {
  return box_compare(a, b, 0);
}

template <class T>
requires std::floating_point<T> constexpr auto
box_y_compare(const std::shared_ptr<hittable<T>> a,
              const std::shared_ptr<hittable<T>> b) -> bool {
  return box_compare(a, b, 1);
}

template <class T>
requires std::floating_point<T> constexpr auto
box_z_compare(const std::shared_ptr<hittable<T>> a,
              const std::shared_ptr<hittable<T>> b) -> bool {
  return box_compare(a, b, 2);
}

#endif
