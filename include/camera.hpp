#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <concepts>
#include <memory>

#include "ray.hpp"

template <class T> requires std::floating_point<T> class camera {
public:
  camera(point3<T> lookfrom, point3<T> lookat, vec3<T> vup,
         T vfov, // vertical field-of-view in degrees
         T aspect_ratio, T aperture, T focus_dist, T start_time = 0,
         T end_time = 0) {
    const auto theta = degrees_to_radians(vfov);
    const auto h = std::tan(theta / 2);
    const auto viewport_height = 2.0 * h;
    const auto viewport_width = aspect_ratio * viewport_height;

    w = unit_vector(lookfrom - lookat);
    u = unit_vector(cross(vup, w));
    v = cross(w, u);

    origin = lookfrom;
    horizontal = focus_dist * viewport_width * u;
    vertical = focus_dist * viewport_height * v;
    lower_left_corner = origin - horizontal / 2 - vertical / 2 - focus_dist * w;

    lens_radius = aperture / 2;

    time0 = start_time;
    time1 = end_time;
  }

  [[nodiscard]] auto get_ray(T s, T t) const -> ray<T> {
    vec3<T> rd = lens_radius * random_in_unit_disk<T>();
    vec3<T> offset = u * rd.x() + v * rd.y();

    return ray(origin + offset,
               lower_left_corner + s * horizontal + t * vertical - origin -
                   offset,
               random_between<T>(time0, time1));
  }

private:
  point3<T> origin;
  point3<T> lower_left_corner;
  vec3<T> horizontal;
  vec3<T> vertical;
  vec3<T> u, v, w;
  T lens_radius;
  T time0, time1; // shutter open/close time
};
#endif
