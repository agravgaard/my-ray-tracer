#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include <memory>
#include <utility>

#include "hittable.hpp"
#include "onb.hpp"
#include "pdf.hpp"
#include "ray.hpp"
#include "texture.hpp"
#include "vec3.hpp"

template <class T> requires std::floating_point<T> struct scatter_record {
  ray<T> specular_ray;
  bool is_specular = false;
  color<T> attenuation;
  std::shared_ptr<pdf<T>> pdf_ptr = nullptr;
};

template <class T> requires std::floating_point<T> class material {
public:
  [[nodiscard]] virtual auto emitted(const ray<T> & /*r_in*/,
                                     const hit_record<T> & /*rec*/, T /*u*/,
                                     T /*v*/, const point3<T> & /*p*/) const
      -> color<T> {
    return color<T>(0, 0, 0);
  }

  virtual auto scatter(const ray<T> & /*r_in*/, const hit_record<T> & /*rec*/,
                       scatter_record<T> & /*srec*/) const -> bool {
    return false;
  };

  [[nodiscard]] virtual auto scattering_pdf(const ray<T> & /*r_in*/,
                                            const hit_record<T> & /*rec*/,
                                            const ray<T> & /*scattered*/) const
      -> T {
    return 0;
  }

  material() = default;
  virtual ~material() = default;
  material(const material &) = default;
  auto operator=(const material &) -> material & = default;
  material(material &&) noexcept = default;
  auto operator=(material &&) noexcept -> material & = default;
};

template <class T>
requires std::floating_point<T> class lambertian : public material<T> {
public:
  explicit lambertian(const color<T> &a)
      : albedo(std::make_shared<solid_color<T>>(a)) {}
  explicit lambertian(std::shared_ptr<texture<T>> a) : albedo(std::move(a)) {}

  auto scatter(const ray<T> & /*r_in*/, const hit_record<T> &rec,
               scatter_record<T> &srec) const -> bool override {
    srec.is_specular = false;
    srec.attenuation = albedo->value(rec.u, rec.v, rec.p);
    srec.pdf_ptr = std::make_shared<cosine_pdf<T>>(rec.normal);
    return true;
  }

  [[nodiscard]] constexpr auto scattering_pdf(const ray<T> & /*r_in*/,
                                              const hit_record<T> &rec,
                                              const ray<T> &scattered) const
      -> T override {
    auto cosine = dot(rec.normal, unit_vector(scattered.direction()));
    return cosine < 0 ? 0 : cosine / pi<T>;
  }

private:
  std::shared_ptr<texture<T>> albedo;
};

template <class T>
requires std::floating_point<T> class metal : public material<T> {
public:
  explicit metal(const color<T> &a, T f) : albedo(a), fuzz(f < 1 ? f : 1) {}

  auto scatter(const ray<T> &r_in, const hit_record<T> &rec,
               scatter_record<T> &srec) const -> bool override {
    vec3 reflected = reflect(unit_vector(r_in.direction()), rec.normal);
    srec.specular_ray =
        ray(rec.p, reflected + fuzz * random_in_unit_sphere<T>(), r_in.time());
    srec.attenuation = albedo;
    srec.is_specular = true;
    srec.pdf_ptr = nullptr;
    return true;
  }

private:
  color<T> albedo;
  T fuzz;
};

template <class T>
requires std::floating_point<T> class dielectric : public material<T> {
public:
  explicit dielectric(T index_of_refraction) : ir(index_of_refraction) {}

  auto scatter(const ray<T> &r_in, const hit_record<T> &rec,
               scatter_record<T> &srec) const -> bool override {
    srec.is_specular = true;
    srec.pdf_ptr = nullptr;
    srec.attenuation = color<T>(1.0, 1.0, 1.0);

    T refraction_ratio = rec.front_face ? (1.0 / ir) : ir;

    vec3 unit_direction = unit_vector(r_in.direction());

    T cos_theta = fmin(dot(-unit_direction, rec.normal), 1.0);
    T sin_theta = sqrt(1.0 - cos_theta * cos_theta);

    bool cannot_refract = refraction_ratio * sin_theta > 1.0;
    vec3<T> direction;

    if (cannot_refract ||
        reflectance(cos_theta, refraction_ratio) > random_real<T>()) {
      direction = reflect(unit_direction, rec.normal);
    } else {
      direction = refract(unit_direction, rec.normal, refraction_ratio);
    }

    srec.specular_ray = ray(rec.p, direction, r_in.time());

    return true;
  }

private:
  T ir; // Index of Refraction

  [[nodiscard]] static constexpr auto reflectance(T cosine, T ref_idx) -> T {
    // Use Schlick's approximation for reflectance.
    auto r0 = (1 - ref_idx) / (1 + ref_idx);
    r0 = r0 * r0;
    constexpr auto five = 5;
    return r0 + (1 - r0) * pow((1 - cosine), five);
  }
};

template <class T>
requires std::floating_point<T> class diffuse_light : public material<T> {
public:
  explicit diffuse_light(std::shared_ptr<texture<T>> a) : emit(a) {}
  explicit diffuse_light(color<T> c)
      : emit(std::make_shared<solid_color<T>>(c)) {}

  auto scatter(const ray<T> & /*r_in*/, const hit_record<T> & /*rec*/,
               scatter_record<T> & /*srec*/) const -> bool override {
    return false;
  }

  [[nodiscard]] auto emitted(const ray<T> & /*r_in*/, const hit_record<T> &rec,
                             T u, T v, const point3<T> &p) const
      -> color<T> override {

    if (rec.front_face) {
      return emit->value(u, v, p);
    }
    return color<T>(0, 0, 0);
  }

private:
  std::shared_ptr<texture<T>> emit;
};

template <class T>
requires std::floating_point<T> class isotropic : public material<T> {
public:
  explicit isotropic(color<T> c)
      : albedo(std::make_shared<solid_color<T>>(c)) {}
  explicit isotropic(std::shared_ptr<texture<T>> a) : albedo(a) {}

  auto scatter(const ray<T> &r_in, const hit_record<T> &rec,
               scatter_record<T> &srec) const -> bool override {
    srec.is_specular = true;
    srec.specular_ray = ray(rec.p, random_in_unit_sphere<T>(), r_in.time());
    srec.attenuation = albedo->value(rec.u, rec.v, rec.p);
    return true;
  }

private:
  std::shared_ptr<texture<T>> albedo;
};

#endif
