#ifndef PERLIN_HPP
#define PERLIN_HPP

#include "vec3.hpp"
#include <algorithm>
#include <memory>
#include <numeric>

constexpr auto default_size = 256;
constexpr auto default_depth = 7;

using uint = unsigned int;

template <class T, size_t N = default_size>
requires std::floating_point<T> class perlin {
public:
  perlin() {
    std::generate(ranfloat.begin(), ranfloat.end(), [&]() mutable {
      return unit_vector(vec3<T>(random_between<T>(-1.0, 1.0),
                                 random_between<T>(-1.0, 1.0),
                                 random_between<T>(-1.0, 1.0)));
    });

    perm_x = perlin_generate_perm();
    perm_y = perlin_generate_perm();
    perm_z = perlin_generate_perm();
  }

  [[nodiscard]] auto noise_old(const point3<T> &p) const -> T {
    constexpr auto uchar_max = 255U;
    auto i = static_cast<uint>(4 * p.x()) & uchar_max;
    auto j = static_cast<uint>(4 * p.y()) & uchar_max;
    auto k = static_cast<uint>(4 * p.z()) & uchar_max;

    return ranfloat.at(perm_x.at(i) ^ perm_y.at(j) ^ perm_z.at(k));
  }

  [[nodiscard]] auto noise(const point3<T> &p) const -> T {
    auto u = p.x() - floor(p.x());
    auto v = p.y() - floor(p.y());
    auto w = p.z() - floor(p.z());

    auto i = static_cast<uint>(floor(p.x()));
    auto j = static_cast<uint>(floor(p.y()));
    auto k = static_cast<uint>(floor(p.z()));

    auto c = array3d<vec3<T>, 3>();

    constexpr auto uchar_max = 255U;

    for (uint di = 0; di < 2; di++) {
      auto &cdi = c.at(di);
      for (uint dj = 0; dj < 2; dj++) {
        auto &cdj = cdi.at(dj);
        for (uint dk = 0; dk < 2; dk++) {
          cdj.at(dk) = ranfloat.at(perm_x.at((i + di) & uchar_max) ^
                                   perm_y.at((j + dj) & uchar_max) ^
                                   perm_z.at((k + dk) & uchar_max));
        }
      }
    }

    return perlin_interp(c, u, v, w);
  }

  [[nodiscard]] constexpr auto turbulence(const point3<T> &p,
                                          int depth = default_depth) const
      -> T {
    auto accum = static_cast<T>(0.0);
    auto temp_p = p;
    auto weight = static_cast<T>(1.0);
    constexpr T half = 0.5;

    for (int i = 0; i < depth; i++) {
      accum += weight * noise(temp_p);
      weight *= half;
      temp_p *= 2;
    }

    return std::fabs(accum);
  }

private:
  std::array<vec3<T>, N> ranfloat;
  std::array<uint, N> perm_x;
  std::array<uint, N> perm_y;
  std::array<uint, N> perm_z;

  auto perlin_generate_perm() -> std::array<uint, N> {
    auto p = std::array<uint, N>();
    std::iota(p.begin(), p.end(), 0);
    permute(p);
    return p;
  }

  void permute(std::array<uint, N> &p) {
    for (uint i = N - 1; i > 0; i--) {
      uint target = static_cast<uint>(random_real<T>() * i + 1);
      std::swap(p.at(i), p.at(target));
    }
  }

  static constexpr auto trilinear_interp(array3d<T, 3> &c, T u, T v, T w) -> T {
    auto accum = 0.0;
    for (int i = 0; i < 2; i++) {
      auto &ci = c.at(i);
      for (int j = 0; j < 2; j++) {
        auto &cj = ci.at(j);
        for (int k = 0; k < 2; k++) {
          accum += (i * u + (1 - i) * (1 - u)) * (j * v + (1 - j) * (1 - v)) *
                   (k * w + (1 - k) * (1 - w)) * cj.at(k);
        }
      }
    }

    return accum;
  }
  static constexpr auto perlin_interp(array3d<vec3<T>, 3> &c, T u, T v, T w)
      -> T {
    auto uu = u * u * (3 - 2 * u);
    auto vv = v * v * (3 - 2 * v);
    auto ww = w * w * (3 - 2 * w);
    auto accum = 0.0;

    for (int i = 0; i < 2; i++) {
      auto &ci = c.at(i);
      for (int j = 0; j < 2; j++) {
        auto &cj = ci.at(j);
        for (int k = 0; k < 2; k++) {
          vec3<T> weight_v(u - i, v - j, w - k);
          accum += (i * uu + (1 - i) * (1 - uu)) *
                   (j * vv + (1 - j) * (1 - vv)) *
                   (k * ww + (1 - k) * (1 - ww)) * dot(cj.at(k), weight_v);
        }
      }
    }

    return accum;
  }
};

#endif
