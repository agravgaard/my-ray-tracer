#ifndef AARECT_HPP
#define AARECT_HPP

#include <concepts>
#include <memory>

#include "hittable.hpp"

constexpr auto padding = 0.0001;

template <class T>
requires std::floating_point<T> class xy_rect : public hittable<T> {
public:
  xy_rect() = default;

  xy_rect(T _x0, T _x1, T _y0, T _y1, T _k, std::shared_ptr<material<T>> mat)
      : x0(_x0), x1(_x1), y0(_y0), y1(_y1), k(_k), mp(std::move(mat)){};

  auto hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
      -> bool override;

  auto bounding_box(T /*time0*/, T /*time1*/, aabb<T> &output_box) const
      -> bool override {
    // The bounding box must have non-zero width in each dimension, so pad the Z
    // dimension a small amount.
    output_box =
        aabb<T>(point3<T>(x0, y0, k - padding), point3<T>(x1, y1, k + padding));
    return true;
  }

private:
  T x0, x1, y0, y1, k;
  std::shared_ptr<material<T>> mp;
};

template <class T>
requires std::floating_point<T> class xz_rect : public hittable<T> {
public:
  xz_rect() = default;

  xz_rect(T _x0, T _x1, T _z0, T _z1, T _k, std::shared_ptr<material<T>> mat)
      : x0(_x0), x1(_x1), z0(_z0), z1(_z1), k(_k), mp(std::move(mat)){};

  auto hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
      -> bool override;

  auto bounding_box(T /*time0*/, T /*time1*/, aabb<T> &output_box) const
      -> bool override {
    // The bounding box must have non-zero width in each dimension, so pad the Y
    // dimension a small amount.
    output_box =
        aabb<T>(point3<T>(x0, k - padding, z0), point3<T>(x1, k + padding, z1));
    return true;
  }

  [[nodiscard]] auto pdf_value(const point3<T> &origin, const vec3<T> &v) const
      -> T override {
    constexpr auto hit_tol = 0.001;

    hit_record<T> rec;
    if (!this->hit(ray(origin, v), hit_tol, infinity<T>, rec)) {
      return 0;
    }

    auto area = (x1 - x0) * (z1 - z0);
    auto distance_squared = rec.t * rec.t * v.length_squared();
    auto cosine = fabs(dot(v, rec.normal) / v.length());

    return distance_squared / (cosine * area);
  }

  [[nodiscard]] auto random(const point3<T> &origin) const -> vec3<T> override {
    auto random_point =
        point3<T>(random_between<T>(x0, x1), k, random_between<T>(z0, z1));
    return random_point - origin;
  }

private:
  T x0, x1, z0, z1, k;
  std::shared_ptr<material<T>> mp;
};

template <class T>
requires std::floating_point<T> class yz_rect : public hittable<T> {
public:
  yz_rect() = default;

  yz_rect(T _y0, T _y1, T _z0, T _z1, T _k, std::shared_ptr<material<T>> mat)
      : y0(_y0), y1(_y1), z0(_z0), z1(_z1), k(_k), mp(std::move(mat)){};

  auto hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
      -> bool override;

  auto bounding_box(T /*time0*/, T /*time1*/, aabb<T> &output_box) const
      -> bool override {
    // The bounding box must have non-zero width in each dimension, so pad the X
    // dimension a small amount.
    output_box =
        aabb<T>(point3<T>(k - padding, y0, z0), point3<T>(k + padding, y1, z1));
    return true;
  }

private:
  T y0, y1, z0, z1, k;
  std::shared_ptr<material<T>> mp;
};

template <class T>
requires std::floating_point<T> auto
xy_rect<T>::hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
    -> bool {
  auto t = (k - r.origin().z()) / r.direction().z();
  if (t < t_min || t > t_max) {
    return false;
  }
  auto x = r.origin().x() + t * r.direction().x();
  auto y = r.origin().y() + t * r.direction().y();
  if (x < x0 || x > x1 || y < y0 || y > y1) {
    return false;
  }
  rec.u = (x - x0) / (x1 - x0);
  rec.v = (y - y0) / (y1 - y0);
  rec.t = t;
  auto outward_normal = vec3<T>(0, 0, 1);
  rec.set_face_normal(r, outward_normal);
  rec.mat = mp;
  rec.p = r.at(t);
  return true;
}

template <class T>
requires std::floating_point<T> auto
xz_rect<T>::hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
    -> bool {
  auto t = (k - r.origin().y()) / r.direction().y();
  if (t < t_min || t > t_max) {
    return false;
  }
  auto x = r.origin().x() + t * r.direction().x();
  auto z = r.origin().z() + t * r.direction().z();
  if (x < x0 || x > x1 || z < z0 || z > z1) {
    return false;
  }
  rec.u = (x - x0) / (x1 - x0);
  rec.v = (z - z0) / (z1 - z0);
  rec.t = t;
  auto outward_normal = vec3<T>(0, 1, 0);
  rec.set_face_normal(r, outward_normal);
  rec.mat = mp;
  rec.p = r.at(t);
  return true;
}

template <class T>
requires std::floating_point<T> auto
yz_rect<T>::hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
    -> bool {
  auto t = (k - r.origin().x()) / r.direction().x();
  if (t < t_min || t > t_max) {
    return false;
  }
  auto y = r.origin().y() + t * r.direction().y();
  auto z = r.origin().z() + t * r.direction().z();
  if (y < y0 || y > y1 || z < z0 || z > z1) {
    return false;
  }
  rec.u = (y - y0) / (y1 - y0);
  rec.v = (z - z0) / (z1 - z0);
  rec.t = t;
  auto outward_normal = vec3<T>(1, 0, 0);
  rec.set_face_normal(r, outward_normal);
  rec.mat = mp;
  rec.p = r.at(t);
  return true;
}

#endif
