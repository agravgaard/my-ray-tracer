#ifndef VEC3_HPP
#define VEC3_HPP

#include <array>
#include <cmath>
#include <concepts>
#include <cstddef>
#include <iostream>

#include "convenience.hpp"

template <class T> requires std::floating_point<T> class vec3 {
public:
  constexpr vec3() = default;
  constexpr vec3(T e0, T e1, T e2) : e{e0, e1, e2} {}

  [[nodiscard]] inline static auto random() -> vec3 {
    return vec3(random_real<T>(), random_real<T>(), random_real<T>());
  }
  [[nodiscard]] inline static auto random(T low, T high) -> vec3 {
    return vec3(random_between<T>(low, high), random_between<T>(low, high),
                random_between<T>(low, high));
  }

  [[nodiscard]] constexpr auto x() const -> T { return e.at(0); }
  [[nodiscard]] constexpr auto y() const -> T { return e.at(1); }
  [[nodiscard]] constexpr auto z() const -> T { return e.at(2); }

  auto constexpr operator-() const -> vec3 {
    return vec3<T>(-e[0], -e[1], -e[2]);
  }
  auto constexpr operator[](int i) const -> T { return e[i]; }
  auto constexpr operator[](int i) -> T & { return e.at(i); }

  constexpr auto operator+=(const vec3 &v) -> vec3 & {
    e[0] += v.e[0];
    e[1] += v.e[1];
    e[2] += v.e[2];
    return *this;
  }

  constexpr auto operator*=(const T t) -> vec3 & {
    e[0] *= t;
    e[1] *= t;
    e[2] *= t;
    return *this;
  }

  constexpr auto operator/=(const T t) -> vec3 & { return *this *= 1 / t; }

  [[nodiscard]] auto length() const -> T { return std::sqrt(length_squared()); }

  [[nodiscard]] constexpr auto length_squared() const -> T {
    return e[0] * e[0] + e[1] * e[1] + e[2] * e[2];
  }

  [[nodiscard]] constexpr auto near_zero() const -> bool {
    // Return true if the vector is close to zero in all dimensions.
    constexpr auto s = 1e-8;
    return (fabs(e[0]) < s) && (fabs(e[1]) < s) && (fabs(e[2]) < s);
  }

private:
  std::array<T, 3> e{0, 0, 0};
};

// Type aliases for vec3
template <class T> using point3 = vec3<T>; // 3D point
template <class T> using color = vec3<T>;  // RGB color

// vec3 Utility Functions

template <class T> inline auto random_cosine_direction() -> vec3<T> {
  const auto r1 = random_real<T>();
  const auto r2 = random_real<T>();
  const auto z = std::sqrt(1 - r2);

  const auto phi = 2 * pi<T> * r1;
  const auto x = std::cos(phi) * std::sqrt(r2);
  const auto y = std::sin(phi) * std::sqrt(r2);

  return vec3(x, y, z);
}

template <class T> auto random_in_unit_sphere() -> vec3<T> {
  while (true) {
    auto p = vec3<T>::random(-1.0, 1.0);
    if (p.length_squared() >= 1) {
      continue;
    }
    return p;
  }
}

template <class T> auto random_unit_vector() -> vec3<T> {
  return unit_vector(random_in_unit_sphere<T>());
}

template <class T> auto random_in_unit_disk() -> vec3<T> {
  while (true) {
    auto p =
        vec3<T>(random_between<T>(-1.0, 1.0), random_between<T>(-1.0, 1.0), 0);
    if (p.length_squared() >= 1) {
      continue;
    }
    return p;
  }
}

template <class T>
constexpr auto reflect(const vec3<T> &v, const vec3<T> &n) -> vec3<T> {
  return v - 2 * dot(v, n) * n;
}

template <class T>
constexpr auto refract(const vec3<T> &uv, const vec3<T> &n, T etai_over_etat)
    -> vec3<T> {
  auto cos_theta = fmin(dot(-uv, n), 1.0);
  vec3 r_out_perp = etai_over_etat * (uv + cos_theta * n);
  vec3 r_out_parallel = -std::sqrt(fabs(1.0 - r_out_perp.length_squared())) * n;
  return r_out_perp + r_out_parallel;
}

template <class T>
constexpr auto operator<<(std::ostream &out, const vec3<T> &v)
    -> std::ostream & {
  return out << v.x() << ' ' << v.y() << ' ' << v.z();
}

template <class T>
constexpr auto operator+(const vec3<T> &u, const vec3<T> &v) -> vec3<T> {
  return vec3<T>(u.x() + v.x(), u.y() + v.y(), u.z() + v.z());
}

template <class T>
constexpr auto operator+(const vec3<T> &u, const T v) -> vec3<T> {
  return vec3<T>(u.x() + v, u.y() + v, u.z() + v);
}

template <class T>
constexpr auto operator-(const vec3<T> &u, const vec3<T> &v) -> vec3<T> {
  return vec3<T>(u.x() - v.x(), u.y() - v.y(), u.z() - v.z());
}

template <class T>
constexpr auto operator*(const vec3<T> &u, const vec3<T> &v) -> vec3<T> {
  return vec3<T>(u.x() * v.x(), u.y() * v.y(), u.z() * v.z());
}

template <class T> constexpr auto operator*(T t, const vec3<T> &v) -> vec3<T> {
  return vec3<T>(t * v.x(), t * v.y(), t * v.z());
}

template <class T> constexpr auto operator*(const vec3<T> &v, T t) -> vec3<T> {
  return t * v;
}

template <class T, class U>
constexpr auto operator/(vec3<T> v, U t) -> vec3<T> {
  return (1 / static_cast<T>(t)) * v;
}

template <class T> constexpr auto dot(const vec3<T> &u, const vec3<T> &v) -> T {
  return u.x() * v.x() + u.y() * v.y() + u.z() * v.z();
}

template <class T>
constexpr auto cross(const vec3<T> &u, const vec3<T> &v) -> vec3<T> {
  return vec3<T>(u.y() * v.z() - u.z() * v.y(), u.z() * v.x() - u.x() * v.z(),
                 u.x() * v.y() - u.y() * v.x());
}

template <class T> constexpr auto unit_vector(vec3<T> v) -> vec3<T> {
  return v / v.length();
}

#endif
