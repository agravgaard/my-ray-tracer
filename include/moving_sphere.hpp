#ifndef MOVING_SPHERE_HPP
#define MOVING_SPHERE_HPP

#include <utility>

#include "hittable.hpp"

template <class T>
requires std::floating_point<T> class moving_sphere : public hittable<T> {
public:
  moving_sphere() = default;
  moving_sphere(point3<T> cen0, point3<T> cen1, T _time0, T _time1, T r,
                std::shared_ptr<material<T>> m)
      : center0(cen0), center1(cen1), time0(_time0), time1(_time1), radius(r),
        mat(std::move(m)){};

  auto hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
      -> bool override;

  auto bounding_box(T t0, T t1, aabb<T> &output_box) const -> bool override;

  auto center(T time) const -> point3<T>;

private:
  point3<T> center0, center1;
  T time0{}, time1{};
  T radius{};
  std::shared_ptr<material<T>> mat;
};

template <class T>
requires std::floating_point<T> auto moving_sphere<T>::center(T time) const
    -> point3<T> {
  return center0 + ((time - time0) / (time1 - time0)) * (center1 - center0);
}

template <class T>
requires std::floating_point<T> auto
moving_sphere<T>::hit(const ray<T> &r, T t_min, T t_max,
                      hit_record<T> &rec) const -> bool {
  vec3 oc = r.origin() - center(r.time());
  auto a = r.direction().length_squared();
  auto half_b = dot(oc, r.direction());
  auto c = oc.length_squared() - radius * radius;

  auto discriminant = half_b * half_b - a * c;
  if (discriminant < 0) {
    return false;
  }
  auto sqrtd = sqrt(discriminant);

  // Find the nearest root that lies in the acceptable range.
  auto root = (-half_b - sqrtd) / a;
  if (root < t_min || t_max < root) {
    root = (-half_b + sqrtd) / a;
    if (root < t_min || t_max < root) {
      return false;
    }
  }

  rec.t = root;
  rec.p = r.at(rec.t);
  auto outward_normal = (rec.p - center(r.time())) / radius;
  rec.set_face_normal(r, outward_normal);
  rec.mat = mat;

  return true;
}

template <class T>
requires std::floating_point<T> auto
moving_sphere<T>::bounding_box(T t0, T t1, aabb<T> &output_box) const -> bool {
  aabb box0(center(t0) - vec3(radius, radius, radius),
            center(t0) + vec3(radius, radius, radius));
  aabb box1(center(t1) - vec3(radius, radius, radius),
            center(t1) + vec3(radius, radius, radius));
  output_box = surrounding_box(box0, box1);
  return true;
}

#endif
