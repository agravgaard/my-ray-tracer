#ifndef EXAMPLE_WORLDS_HPP
#define EXAMPLE_WORLDS_HPP

#include <concepts>
#include <cstdlib>
#include <memory>

#include "aarect.hpp"
#include "box.hpp"
#include "bvh.hpp"
#include "constant_medium.hpp"
#include "hittable_list.hpp"
#include "material.hpp"
#include "moving_sphere.hpp"
#include "sphere.hpp"

constexpr auto p0001 = .0001;
constexpr auto p01 = .01;
constexpr auto p05 = .05;
constexpr auto p12 = .12;
constexpr auto p15 = .15;
constexpr auto p45 = .45;
constexpr auto p48 = .48;
constexpr auto p53 = .53;
constexpr auto p65 = .65;
constexpr auto p73 = .73;
constexpr auto p83 = .83;
constexpr auto p85 = .85;
constexpr auto p88 = .88;

constexpr auto d1p5 = 1.5;
constexpr auto d5000 = 5000.0;

constexpr auto i7 = 7;
constexpr auto i10 = 10;
constexpr auto i15 = 15;
constexpr auto i18 = 18;
constexpr auto i30 = 30;
constexpr auto i45 = 45;
constexpr auto i50 = 50;
constexpr auto i65 = 65;
constexpr auto i70 = 70;
constexpr auto i80 = 80;
constexpr auto i90 = 90;
constexpr auto i100 = 100;
constexpr auto i101 = 101;
constexpr auto i113 = 113;
constexpr auto i123 = 123;
constexpr auto i127 = 127;
constexpr auto i130 = 130;
constexpr auto i145 = 145;
constexpr auto i147 = 147;
constexpr auto i150 = 150;
constexpr auto i165 = 165;
constexpr auto i190 = 190;
constexpr auto i200 = 200;
constexpr auto i213 = 213;
constexpr auto i220 = 220;
constexpr auto i227 = 227;
constexpr auto i260 = 260;
constexpr auto i265 = 265;
constexpr auto i270 = 270;
constexpr auto i280 = 280;
constexpr auto i295 = 295;
constexpr auto i300 = 300;
constexpr auto i330 = 330;
constexpr auto i332 = 332;
constexpr auto i343 = 343;
constexpr auto i360 = 360;
constexpr auto i395 = 395;
constexpr auto i400 = 400;
constexpr auto i412 = 412;
constexpr auto i423 = 423;
constexpr auto i432 = 432;
constexpr auto i443 = 443;
constexpr auto i554 = 554;
constexpr auto i555 = 555;

template <class T>
requires std::floating_point<T> auto final_book2() -> hittable_list<T> {
  hittable_list<T> boxes1;

  auto ground = std::make_shared<lambertian<T>>(color<T>(p48, p83, p53));

  const int boxes_per_side = 20;
  for (int i = 0; i < boxes_per_side; i++) {
    for (int j = 0; j < boxes_per_side; j++) {
      auto w = 100.0;
      auto x0 = -thousand + i * w;
      auto z0 = -thousand + j * w;
      auto y0 = 0.0;
      auto x1 = x0 + w;
      auto y1 = random_between<T>(1, i101);
      auto z1 = z0 + w;

      boxes1.add(std::make_shared<box<T>>(point3<T>(x0, y0, z0),
                                          point3<T>(x1, y1, z1), ground));
    }
  }

  hittable_list<T> objects;

  objects.add(std::make_shared<bvh_node<T>>(boxes1, 0, 1));

  auto light = std::make_shared<diffuse_light<T>>(color<T>(i7, i7, i7));
  objects.add(
      std::make_shared<xz_rect<T>>(i123, i423, i147, i412, i554, light));

  auto center1 = point3<T>(i400, i400, i200);
  auto center2 = center1 + vec3<T>(i30, 0, 0);
  auto moving_sphere_material =
      std::make_shared<lambertian<T>>(color<T>(p7, p3, p1));
  objects.add(std::make_shared<moving_sphere<T>>(center1, center2, 0, 1, i50,
                                                 moving_sphere_material));

  objects.add(std::make_shared<sphere<T>>(
      point3<T>(i260, i150, i45), i50, std::make_shared<dielectric<T>>(d1p5)));
  objects.add(std::make_shared<sphere<T>>(
      point3<T>(0, i150, i145), i50,
      std::make_shared<metal<T>>(color<T>(p8, p8, p9), 1.0)));

  auto boundary = std::make_shared<sphere<T>>(
      point3<T>(i360, i150, i145), i70, std::make_shared<dielectric<T>>(d1p5));
  objects.add(boundary);
  objects.add(
      std::make_shared<constant_medium<T>>(boundary, p2, color<T>(p2, p4, p9)));
  boundary = std::make_shared<sphere<T>>(point3<T>(0, 0, 0), d5000,
                                         std::make_shared<dielectric<T>>(d1p5));
  objects.add(
      std::make_shared<constant_medium<T>>(boundary, p0001, color<T>(1, 1, 1)));

  auto emat = std::make_shared<lambertian<T>>(
      std::make_shared<image_texture<T>>("assets/earthmap.jpg"));
  objects.add(
      std::make_shared<sphere<T>>(point3<T>(i400, i200, i400), i100, emat));
  auto pertext = std::make_shared<noise_texture<T>>(p1);
  objects.add(
      std::make_shared<sphere<T>>(point3<T>(i220, i280, i300), i80,
                                  std::make_shared<lambertian<T>>(pertext)));

  hittable_list<T> boxes2;
  auto white = std::make_shared<lambertian<T>>(color<T>(p73, p73, p73));
  for (int j = 0; j < thousand; j++) {
    boxes2.add(
        std::make_shared<sphere<T>>(point3<T>::random(0, i165), i10, white));
  }

  objects.add(std::make_shared<translate<T>>(
      std::make_shared<rotate_y<T>>(
          std::make_shared<bvh_node<T>>(boxes2, 0.0, 1.0), i15),
      vec3<T>(-i100, i270, i395)));

  return objects;
}

template <class T>
requires std::floating_point<T> auto cornell_smoke() -> hittable_list<T> {
  hittable_list<T> objects;

  auto red = std::make_shared<lambertian<T>>(color<T>(p65, p05, p05));
  auto white = std::make_shared<lambertian<T>>(color<T>(p73, p73, p73));
  auto green = std::make_shared<lambertian<T>>(color<T>(p12, p45, p15));
  auto light = std::make_shared<diffuse_light<T>>(color<T>(i7, i7, i7));

  objects.add(std::make_shared<yz_rect<T>>(0, i555, 0, i555, i555, green));
  objects.add(std::make_shared<yz_rect<T>>(0, i555, 0, i555, 0, red));
  objects.add(
      std::make_shared<xz_rect<T>>(i113, i443, i127, i432, i554, light));
  objects.add(std::make_shared<xz_rect<T>>(0, i555, 0, i555, i555, white));
  objects.add(std::make_shared<xz_rect<T>>(0, i555, 0, i555, 0, white));
  objects.add(std::make_shared<xy_rect<T>>(0, i555, 0, i555, i555, white));

  std::shared_ptr<hittable<T>> box1 = std::make_shared<box<T>>(
      point3<T>(0, 0, 0), point3<T>(i165, i330, i165), white);
  box1 = std::make_shared<rotate_y<T>>(box1, i15);
  box1 = std::make_shared<translate<T>>(box1, vec3<T>(i265, 0, i295));

  std::shared_ptr<hittable<T>> box2 = std::make_shared<box<T>>(
      point3<T>(0, 0, 0), point3<T>(i165, i165, i165), white);
  box2 = std::make_shared<rotate_y<T>>(box2, -i18);
  box2 = std::make_shared<translate<T>>(box2, vec3<T>(i130, 0, i65));

  objects.add(
      std::make_shared<constant_medium<T>>(box1, p01, color<T>(0, 0, 0)));
  objects.add(
      std::make_shared<constant_medium<T>>(box2, p01, color<T>(1, 1, 1)));

  return objects;
}

template <class T>
requires std::floating_point<T> auto cornell_box() -> hittable_list<T> {
  hittable_list<T> objects;

  auto red = std::make_shared<lambertian<T>>(color<T>(p65, p05, p05));

  auto white = std::make_shared<lambertian<T>>(color<T>(p73, p73, p73));

  auto green = std::make_shared<lambertian<T>>(color<T>(p12, p45, p15));

  auto light = std::make_shared<diffuse_light<T>>(color<T>(i15, i15, i15));

  objects.add(std::make_shared<yz_rect<T>>(0, i555, 0, i555, i555, green));
  objects.add(std::make_shared<yz_rect<T>>(0, i555, 0, i555, 0, red));
  objects.add(std::make_shared<flip_face<T>>(
      std::make_shared<xz_rect<T>>(i213, i343, i227, i332, i555 - 1, light)));
  objects.add(std::make_shared<xz_rect<T>>(0, i555, 0, i555, 0, white));
  objects.add(std::make_shared<xz_rect<T>>(0, i555, 0, i555, i555, white));
  objects.add(std::make_shared<xy_rect<T>>(0, i555, 0, i555, i555, white));

  auto aluminum = std::make_shared<metal<T>>(color<T>(p8, p85, p88), 0.0);
  std::shared_ptr<hittable<T>> box1 = std::make_shared<box<T>>(
      point3<T>(0, 0, 0), point3<T>(i165, i330, i165), aluminum);
  box1 = std::make_shared<rotate_y<T>>(box1, i15);
  box1 = std::make_shared<translate<T>>(box1, vec3<T>(i265, 0, i295));
  objects.add(box1);

  auto glass = std::make_shared<dielectric<T>>(d1p5);
  objects.add(
      std::make_shared<sphere<T>>(point3<T>(i190, i90, i190), i90, glass));

  return objects;
}

template <class T>
requires std::floating_point<T> auto simple_light() -> hittable_list<T> {
  hittable_list<T> objects;

  auto pertext = std::make_shared<noise_texture<T>>(4);
  objects.add(
      std::make_shared<sphere<T>>(point3<T>(0, -thousand, 0), thousand,
                                  std::make_shared<lambertian<T>>(pertext)));
  objects.add(std::make_shared<sphere<T>>(
      point3<T>(0, 2, 0), 2, std::make_shared<lambertian<T>>(pertext)));

  auto difflight = std::make_shared<diffuse_light<T>>(color<T>(4, 4, 4));
  objects.add(
      std::make_shared<sphere<T>>(point3<T>(0, p7 * ten, 0), 2, difflight));
  objects.add(std::make_shared<xy_rect<T>>(3, five, 1, 3, -2, difflight));

  return objects;
}

template <class T>
requires std::floating_point<T> auto earth() -> hittable_list<T> {
  auto earth_texture =
      std::make_shared<image_texture<T>>("assets/earthmap.jpg");
  auto earth_surface = std::make_shared<lambertian<T>>(earth_texture);
  auto globe =
      std::make_shared<sphere<T>>(point3<T>(0, 0, 0), 2, earth_surface);

  return hittable_list<T>(globe);
}

template <class T>
requires std::floating_point<T> auto two_perlin_spheres() -> hittable_list<T> {
  hittable_list<T> objects;

  auto pertext = std::make_shared<noise_texture<T>>(4);
  objects.add(
      std::make_shared<sphere<T>>(point3<T>(0, -thousand, 0), thousand,
                                  std::make_shared<lambertian<T>>(pertext)));
  objects.add(std::make_shared<sphere<T>>(
      point3<T>(0, 2, 0), 2, std::make_shared<lambertian<T>>(pertext)));

  return objects;
}

template <class T>
requires std::floating_point<T> auto two_spheres() -> hittable_list<T> {
  hittable_list<T> objects;

  auto checker = std::make_shared<checker_texture<T>>(color<T>(p2, p3, p1),
                                                      color<T>(p9, p9, p9));

  objects.add(std::make_shared<sphere<T>>(
      point3<T>(0, -ten, 0), ten, std::make_shared<lambertian<T>>(checker)));
  objects.add(std::make_shared<sphere<T>>(
      point3<T>(0, ten, 0), ten, std::make_shared<lambertian<T>>(checker)));

  return objects;
}

template <class T>
requires std::floating_point<T> auto random_scene() -> hittable_list<T> {
  hittable_list<T> world;

  auto checker = std::make_shared<checker_texture<T>>(color<T>(p2, p3, p1),
                                                      color<T>(p9, p9, p9));
  world.add(
      std::make_shared<sphere<T>>(point3<T>(0, -thousand, 0), thousand,
                                  std::make_shared<lambertian<T>>(checker)));

  constexpr auto eleven = 11;

  for (int a = -eleven; a < eleven; a++) {
    for (int b = -eleven; b < eleven; b++) {
      auto choose_mat = random_real<T>();
      point3<T> center(a + p9 * random_real<T>(), p2,
                       b + p9 * random_real<T>());

      if ((center - point3<T>(4, p2, 0)).length() > p9) {
        std::shared_ptr<material<T>> sphere_material;

        if (choose_mat < p8) {
          // diffuse
          auto albedo = color<T>::random() * color<T>::random();
          sphere_material = std::make_shared<lambertian<T>>(albedo);

          auto center2 = center + vec3<T>(0, random_real<T>() / 2, 0);
          world.add(std::make_shared<moving_sphere<T>>(
              center, center2, 0.0, 1.0, p2, sphere_material));
        } else if (choose_mat < p95) {
          // metal
          auto albedo = (color<T>::random() / 2) + half;
          auto fuzz = random_real<T>() / 2;
          sphere_material = std::make_shared<metal<T>>(albedo, fuzz);
          world.add(std::make_shared<sphere<T>>(center, p2, sphere_material));
        } else {
          // glass
          sphere_material = std::make_shared<dielectric<T>>(1 + half);
          world.add(std::make_shared<sphere<T>>(center, p2, sphere_material));
        }
      }
    }
  }

  auto material1 = std::make_shared<dielectric<T>>(1 + half);
  world.add(std::make_shared<sphere<T>>(point3<T>(0, 1, 0), 1.0, material1));

  auto material2 = std::make_shared<lambertian<T>>(color<T>(p4, p2, p1));
  world.add(std::make_shared<sphere<T>>(point3<T>(-4, 1, 0), 1.0, material2));

  auto material3 = std::make_shared<metal<T>>(color<T>(p7, p6, half), 0.0);
  world.add(std::make_shared<sphere<T>>(point3<T>(4, 1, 0), 1.0, material3));

  return world;
}

#endif
