#ifndef SPHERE_HPP
#define SPHERE_HPP

#include <utility>

#include "hittable.hpp"
#include "onb.hpp"
#include "vec3.hpp"

template <class T>
requires std::floating_point<T> class sphere : public hittable<T> {
public:
  constexpr sphere() = default;
  constexpr sphere(point3<T> cen, T r, std::shared_ptr<material<T>> m)
      : center(cen), radius(r), mat(std::move(m)){};

  constexpr auto hit(const ray<T> &r, T t_min, T t_max,
                     hit_record<T> &rec) const -> bool override;

  constexpr auto bounding_box(T time0, T time1, aabb<T> &output_box) const
      -> bool override;

  [[nodiscard]] auto random(const point3<T> &o) const -> vec3<T> override;
  [[nodiscard]] constexpr auto pdf_value(const point3<T> &o,
                                         const vec3<T> &v) const -> T override;

private:
  static void get_sphere_uv(const point3<T> &p, T &u, T &v) {
    // p: a given point on the sphere of radius one, centered at the origin.
    // u: returned value [0,1] of angle around the Y axis from X=-1.
    // v: returned value [0,1] of angle from Y=-1 to Y=+1.
    // <1 0 0> yields <0.50 0.50>         <-1  0  0> yields <0.00 0.50>
    // <0 1 0> yields <0.50 1.00>         < 0 -1  0> yields <0.50 0.00>
    // <0 0 1> yields <0.25 0.50>         < 0  0 -1> yields <0.75 0.50>

    auto theta = acos(-p.y());
    auto phi = atan2(-p.z(), p.x()) + pi<T>;

    u = phi / (2 * pi<T>);
    v = theta / pi<T>;
  }

  point3<T> center;
  T radius;
  std::shared_ptr<material<T>> mat;
};

template <class T>
requires std::floating_point<T> constexpr auto
sphere<T>::hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
    -> bool {
  vec3 oc = r.origin() - center;
  auto a = r.direction().length_squared();
  auto half_b = dot(oc, r.direction());
  auto c = oc.length_squared() - radius * radius;

  auto discriminant = half_b * half_b - a * c;
  if (discriminant < 0) {
    return false;
  }
  auto sqrtd = sqrt(discriminant);

  // Find the nearest root that lies in the acceptable range.
  auto root = (-half_b - sqrtd) / a;
  if (root < t_min || t_max < root) {
    root = (-half_b + sqrtd) / a;
    if (root < t_min || t_max < root) {
      return false;
    }
  }

  rec.t = root;
  rec.p = r.at(rec.t);
  vec3 outward_normal = (rec.p - center) / radius;
  rec.set_face_normal(r, outward_normal);
  get_sphere_uv(outward_normal, rec.u, rec.v);
  rec.mat = mat;

  return true;
}

template <class T>
requires std::floating_point<T> constexpr auto
sphere<T>::bounding_box(T /*time0*/, T /*time1*/, aabb<T> &output_box) const
    -> bool {
  output_box = aabb(center - vec3(radius, radius, radius),
                    center + vec3(radius, radius, radius));
  return true;
}

template <class T>
requires std::floating_point<T> constexpr auto
sphere<T>::pdf_value(const point3<T> &o, const vec3<T> &v) const -> T {
  hit_record<T> rec;
  constexpr auto hit_tol = 0.001;
  if (!this->hit(ray(o, v), hit_tol, infinity<T>, rec)) {
    return 0;
  }

  auto cos_theta_max =
      sqrt(1 - radius * radius / (center - o).length_squared());
  auto solid_angle = 2 * pi<T> * (1 - cos_theta_max);

  return 1 / solid_angle;
}

template <class T>
requires std::floating_point<T> auto sphere<T>::random(const point3<T> &o) const
    -> vec3<T> {
  vec3<T> direction = center - o;
  auto distance_squared = direction.length_squared();
  onb<T> uvw;
  uvw.build_from_w(direction);
  return uvw.local(random_to_sphere(radius, distance_squared));
}

#endif
