#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <memory>

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

#include "perlin.hpp"
#include "vec3.hpp"

template <class T> requires std::floating_point<T> class texture {
public:
  texture() = default;
  virtual ~texture() = default;
  texture(const texture &) = default;
  auto operator=(const texture &) -> texture & = default;
  texture(texture &&) noexcept = default;
  auto operator=(texture &&) noexcept -> texture & = default;

  [[nodiscard]] virtual auto value(T u, T v, const point3<T> &p) const
      -> color<T> = 0;
};

template <class T>
requires std::floating_point<T> class solid_color : public texture<T> {
public:
  solid_color() = default;
  explicit solid_color(color<T> c) : color_value(c) {}

  solid_color(T red, T green, T blue)
      : solid_color(color<T>(red, green, blue)) {}

  [[nodiscard]] auto value(T /*u*/, T /*v*/, const vec3<T> & /*p*/) const
      -> color<T> override {
    return color_value;
  }

private:
  color<T> color_value;
};

template <class T>
requires std::floating_point<T> class checker_texture : public texture<T> {
public:
  checker_texture() = default;

  checker_texture(std::shared_ptr<texture<T>> _even,
                  std::shared_ptr<texture<T>> _odd)
      : even(_even), odd(_odd) {}

  checker_texture(color<T> c1, color<T> c2)
      : even(std::make_shared<solid_color<T>>(c1)),
        odd(std::make_shared<solid_color<T>>(c2)) {}

  [[nodiscard]] auto value(T u, T v, const point3<T> &p) const
      -> color<T> override {
    constexpr T ten = 10;
    auto sines = sin(ten * p.x()) * sin(ten * p.y()) * sin(ten * p.z());
    if (sines < 0) {
      return odd->value(u, v, p);
    }
    // else
    return even->value(u, v, p);
  }

  std::shared_ptr<texture<T>> even;
  std::shared_ptr<texture<T>> odd;
};

template <class T>
requires std::floating_point<T> class noise_texture : public texture<T> {
public:
  noise_texture() = default;
  explicit noise_texture(T sc) : scale(sc) {}

  [[nodiscard]] auto value(T /*u*/, T /*v*/, const point3<T> &p) const
      -> color<T> override {
    /* Marble */
    constexpr T half = 0.5;
    constexpr T ten = 10;
    return color<T>(1, 1, 1) * half *
           (1 + sin(scale * p.z() + ten * noise.turbulence(p)));
    /* Stone camo:
    return color<T>(1, 1, 1) * noise.turbulence(scale * p);
    */
    /* blurred noise:
     * constexpr T half = 0.5;
     * constexpr T one = 1.0;
     * return color<T>(1, 1, 1) * half * (one + noise.noise(scale * p));
     */
  }

private:
  perlin<T> noise{};
  T scale;
};

template <class T>
requires std::floating_point<T> class image_texture : public texture<T> {
public:
  constexpr static int bytes_per_pixel = 3;

  image_texture() = default;

  explicit image_texture(const char *filename) {
    auto components_per_pixel = bytes_per_pixel;

    auto *p_data = stbi_load(filename, &width, &height, &components_per_pixel,
                             components_per_pixel);

    if (p_data == nullptr) {
      std::cerr << "ERROR: Could not load texture image file '" << filename
                << "'.\n";
      width = height = 0;
    } else {
      std::copy_n(p_data, width * height * components_per_pixel,
                  std::back_inserter(data));

      bytes_per_scanline = bytes_per_pixel * width;
    }
  }

  [[nodiscard]] auto value(T u, T v, const vec3<T> & /*p*/) const
      -> color<T> override {
    // If we have no texture data, then return solid cyan as a debugging aid.
    if (data.empty()) {
      return color<T>(0, 1, 1);
    }

    // Clamp input texture coordinates to [0,1] x [1,0]
    u = std::clamp(u, 0.0, 1.0);
    v = 1.0 - std::clamp(v, 0.0, 1.0); // Flip V to image coordinates

    auto i = static_cast<int>(u * width);
    auto j = static_cast<int>(v * height);

    // Clamp integer mapping, since actual coordinates should be less than 1.0
    if (i >= width) {
      i = width - 1;
    }
    if (j >= height) {
      j = height - 1;
    }

    constexpr auto color_scale = 1.0 / 255.0;
    const auto index = j * bytes_per_scanline + i * bytes_per_pixel;

    return color<T>(color_scale * data.at(index),
                    color_scale * data.at(index + 1),
                    color_scale * data.at(index + 2));
  }

private:
  std::vector<unsigned char> data;
  int width{}, height{};
  int bytes_per_scanline{};
};

#endif
