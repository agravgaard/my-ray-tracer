#ifndef AABB_HPP
#define AABB_HPP

#include <ray.hpp>
#include <vec3.hpp>

template <class T> requires std::floating_point<T> class aabb {
public:
  constexpr aabb() = default;
  constexpr aabb(const point3<T> &a, const point3<T> &b) {
    minimum = a;
    maximum = b;
  }

  [[nodiscard]] constexpr auto min() const -> point3<T> { return minimum; }
  [[nodiscard]] constexpr auto max() const -> point3<T> { return maximum; }

  [[nodiscard]] constexpr auto hit_simple(const ray<T> &r, T t_min,
                                          T t_max) const -> bool {
    for (int a = 0; a < 3; a++) {
      auto t0 = fmin((minimum[a] - r.origin()[a]) / r.direction()[a],
                     (maximum[a] - r.origin()[a]) / r.direction()[a]);
      auto t1 = fmax((minimum[a] - r.origin()[a]) / r.direction()[a],
                     (maximum[a] - r.origin()[a]) / r.direction()[a]);
      t_min = fmax(t0, t_min);
      t_max = fmin(t1, t_max);
      if (t_max <= t_min) {
        return false;
      }
    }
    return true;
  }

  // Fast version
  [[nodiscard]] constexpr auto hit(const ray<T> &r, T t_min, T t_max) const
      -> bool {
    for (int a = 0; a < 3; a++) {
      const auto invD = static_cast<T>(1) / r.direction()[a];
      auto t0 = (min()[a] - r.origin()[a]) * invD;
      auto t1 = (max()[a] - r.origin()[a]) * invD;
      if (invD < static_cast<T>(0)) {
        std::swap(t0, t1);
      }
      t_min = t0 > t_min ? t0 : t_min;
      t_max = t1 < t_max ? t1 : t_max;
      if (t_max <= t_min) {
        return false;
      }
    }
    return true;
  }

private:
  point3<T> minimum;
  point3<T> maximum;
};

template <class T>
requires std::floating_point<T> constexpr auto surrounding_box(aabb<T> box0,
                                                               aabb<T> box1)
    -> aabb<T> {
  point3<T> small(std::fmin(box0.min().x(), box1.min().x()),
                  std::fmin(box0.min().y(), box1.min().y()),
                  std::fmin(box0.min().z(), box1.min().z()));

  point3<T> big(std::fmax(box0.max().x(), box1.max().x()),
                std::fmax(box0.max().y(), box1.max().y()),
                std::fmax(box0.max().z(), box1.max().z()));

  return aabb(small, big);
}

#endif
