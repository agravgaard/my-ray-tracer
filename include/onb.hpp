#ifndef ONB_HPP
#define ONB_HPP

#include "vec3.hpp"

template <class T> requires std::floating_point<T> class onb {
public:
  onb() = default;

  constexpr auto operator[](int i) const -> vec3<T> { return axis.at(i); }

  [[nodiscard]] auto u() const -> vec3<T> { return axis[0]; }
  [[nodiscard]] auto v() const -> vec3<T> { return axis[1]; }
  [[nodiscard]] auto w() const -> vec3<T> { return axis[2]; }

  [[nodiscard]] auto local(double a, double b, double c) const -> vec3<T> {
    return a * u() + b * v() + c * w();
  }

  [[nodiscard]] auto local(const vec3<T> &a) const -> vec3<T> {
    return a.x() * u() + a.y() * v() + a.z() * w();
  }

  void build_from_w(const vec3<T> &n);

private:
  std::array<vec3<T>, 3> axis;
};

template <class T>
requires std::floating_point<T> void onb<T>::build_from_w(const vec3<T> &n) {
  axis[2] = unit_vector(n);
  constexpr auto p9 = 0.9;
  vec3<T> a = (fabs(w().x()) > p9) ? vec3<T>(0, 1, 0) : vec3<T>(1, 0, 0);
  axis[1] = unit_vector(cross(w(), a));
  axis[0] = cross(w(), v());
}

#endif
