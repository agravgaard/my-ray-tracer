#ifndef PDF_HPP
#define PDF_HPP

#include <concepts>
#include <functional>
#include <memory>
#include <utility>

#include "hittable_list.hpp"
#include "onb.hpp"
#include "vec3.hpp"

template <class T> requires std::floating_point<T> class pdf {
public:
  pdf() = default;
  virtual ~pdf() = default;
  pdf(const pdf &) = default;
  auto operator=(const pdf &) -> pdf & = default;
  pdf(pdf &&) noexcept = default;
  auto operator=(pdf &&) noexcept -> pdf & = default;

  [[nodiscard]] virtual auto value(const vec3<T> &direction) const -> T = 0;
  [[nodiscard]] virtual auto generate() const -> vec3<T> = 0;
};

template <class T>
requires std::floating_point<T> class cosine_pdf : public pdf<T> {
public:
  explicit cosine_pdf(const vec3<T> &w) { uvw.build_from_w(w); }

  [[nodiscard]] auto value(const vec3<T> &direction) const -> T override {
    auto cosine = dot(unit_vector(direction), uvw.w());
    return (cosine <= 0) ? 0 : cosine / pi<T>;
  }

  [[nodiscard]] auto generate() const -> vec3<T> override {
    return uvw.local(random_cosine_direction<T>());
  }

private:
  onb<T> uvw;
};

template <class T>
requires std::floating_point<T> class hittable_pdf : public pdf<T> {
public:
  hittable_pdf(std::shared_ptr<hittable<T>> p, const point3<T> &origin)
      : ptr(std::move(p)), o(origin) {}

  [[nodiscard]] auto value(const vec3<T> &direction) const -> T override {
    return ptr->pdf_value(o, direction);
  }

  [[nodiscard]] auto generate() const -> vec3<T> override {
    return ptr->random(o);
  }

private:
  std::shared_ptr<hittable<T>> ptr;
  point3<T> o;
};

template <class T>
requires std::floating_point<T> class mixture_pdf : public pdf<T> {
public:
  mixture_pdf(std::shared_ptr<pdf<T>> p0, std::shared_ptr<pdf<T>> p1) {
    p.at(0) = p0;
    p.at(1) = p1;
  }

  [[nodiscard]] auto value(const vec3<T> &direction) const -> T override {
    return half * p.at(0)->value(direction) + half * p.at(1)->value(direction);
  }

  [[nodiscard]] auto generate() const -> vec3<T> override {
    if (random_real<T>() < half) {
      return p.at(0)->generate();
    }
    return p.at(1)->generate();
  }

private:
  std::array<std::shared_ptr<pdf<T>>, 2> p;
};

template <class T>
requires std::floating_point<T> inline auto random_to_sphere(T radius,
                                                             T distance_squared)
    -> vec3<T> {
  auto r1 = random_real<T>();
  auto r2 = random_real<T>();
  auto z = 1 + r2 * (std::sqrt(1 - radius * radius / distance_squared) - 1);

  auto phi = 2 * pi<T> * r1;
  auto x = std::cos(phi) * std::sqrt(1 - z * z);
  auto y = std::sin(phi) * std::sqrt(1 - z * z);

  return vec3<T>(x, y, z);
}

#endif
