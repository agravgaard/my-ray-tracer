#ifndef CONSTANT_MEDIUM_HPP
#define CONSTANT_MEDIUM_HPP

#include <concepts>
#include <utility>

#include "hittable.hpp"
#include "material.hpp"
#include "texture.hpp"

template <class T>
requires std::floating_point<T> class constant_medium : public hittable<T> {
public:
  constant_medium(std::shared_ptr<hittable<T>> b, T d,
                  std::shared_ptr<texture<T>> a)
      : boundary(b), neg_inv_density(-1 / d),
        phase_function(std::make_shared<isotropic<T>>(a)) {}

  constant_medium(std::shared_ptr<hittable<T>> b, T d, color<T> c)
      : boundary(std::move(b)), neg_inv_density(-1 / d),
        phase_function(std::make_shared<isotropic<T>>(c)) {}

  auto hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
      -> bool override;

  auto bounding_box(T time0, T time1, aabb<T> &output_box) const
      -> bool override {
    return boundary->bounding_box(time0, time1, output_box);
  }

private:
  std::shared_ptr<hittable<T>> boundary;
  T neg_inv_density;
  std::shared_ptr<material<T>> phase_function;
};

template <class T>
requires std::floating_point<T> auto
constant_medium<T>::hit(const ray<T> &r, T t_min, T t_max,
                        hit_record<T> &rec) const -> bool {
  // Print occasional samples when debugging. To enable, set enableDebug true.
  constexpr bool enableDebug = false;
  constexpr auto debug_tolerance = 0.00001;
  const bool debugging = enableDebug && random_real<T>() < debug_tolerance;

  hit_record<T> rec1;
  hit_record<T> rec2;

  if (!boundary->hit(r, -infinity<T>, infinity<T>, rec1)) {
    return false;
  }

  constexpr auto tolerance = 0.0001;
  if (!boundary->hit(r, rec1.t + tolerance, infinity<T>, rec2)) {
    return false;
  }

  if constexpr (debugging) {
    std::cerr << "\nt_min=" << rec1.t << ", t_max=" << rec2.t << '\n';
  }

  if (rec1.t < t_min) {
    rec1.t = t_min;
  }
  if (rec2.t > t_max) {
    rec2.t = t_max;
  }

  if (rec1.t >= rec2.t) {
    return false;
  }

  if (rec1.t < 0) {
    rec1.t = 0;
  }

  const auto ray_length = r.direction().length();
  const auto distance_inside_boundary = (rec2.t - rec1.t) * ray_length;
  const auto hit_distance = neg_inv_density * log(random_real<T>());

  if (hit_distance > distance_inside_boundary) {
    return false;
  }

  rec.t = rec1.t + hit_distance / ray_length;
  rec.p = r.at(rec.t);

  if constexpr (debugging) {
    std::cerr << "hit_distance = " << hit_distance << '\n'
              << "rec.t = " << rec.t << '\n'
              << "rec.p = " << rec.p << '\n';
  }

  rec.normal = vec3<T>(1, 0, 0); // arbitrary
  rec.front_face = true;         // also arbitrary
  rec.mat = phase_function;

  return true;
}

#endif
