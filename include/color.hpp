#ifndef COLOR_HPP
#define COLOR_HPP

#include "vec3.hpp"

#include <algorithm>
#include <string>

template <class T>
auto write_color(color<T> pixel_color, int samples_per_pixel) -> std::string {
  constexpr auto pixelmax = 256;
  constexpr auto lt_one = .999;
  auto r = pixel_color.x();
  auto g = pixel_color.y();
  auto b = pixel_color.z();

  // Divide the color by the number of samples.
  auto scale = 1.0 / samples_per_pixel;
  r = std::sqrt(scale * r);
  g = std::sqrt(scale * g);
  b = std::sqrt(scale * b);

  // Write the translated [0,255] value of each color component.
  std::string s =
      std::to_string(static_cast<int>(pixelmax * std::clamp(r, 0.0, lt_one))) +
      ' ' +
      std::to_string(static_cast<int>(pixelmax * std::clamp(g, 0.0, lt_one))) +
      ' ' +
      std::to_string(static_cast<int>(pixelmax * std::clamp(b, 0.0, lt_one))) +
      '\n';
  return s;
}

template <int N>
void write_image(std::ostream &out, std::array<std::string, N> img) {
  for (const auto &s : img) {
    out << s;
  }
}

#endif
