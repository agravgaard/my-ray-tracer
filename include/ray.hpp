#ifndef RAY_HPP
#define RAY_HPP

#include <concepts>

#include "vec3.hpp"

template <class T> requires std::floating_point<T> class ray {
public:
  constexpr ray() = default;
  constexpr ray(const point3<T> &origin, const vec3<T> &direction, T time = 0.0)
      : orig(origin), dir(direction), tm(time) {}

  [[nodiscard]] constexpr auto origin() const -> point3<T> { return orig; }
  [[nodiscard]] constexpr auto direction() const -> vec3<T> { return dir; }
  [[nodiscard]] constexpr auto time() const -> T { return tm; }

  [[nodiscard]] constexpr auto at(T t) const -> point3<T> {
    return orig + t * dir;
  }

private:
  point3<T> orig;
  vec3<T> dir;
  T tm{};
};

#endif
