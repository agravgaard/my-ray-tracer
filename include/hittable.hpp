#ifndef HITTABLE_HPP
#define HITTABLE_HPP

#include <memory>
#include <utility>

#include "aabb.hpp"
#include "ray.hpp"

template <class T> requires std::floating_point<T> class material;

template <class T> requires std::floating_point<T> struct hit_record {
public:
  point3<T> p;
  vec3<T> normal;
  std::shared_ptr<material<T>> mat;
  T t{};
  T u{};
  T v{};
  bool front_face{};

  constexpr void set_face_normal(const ray<T> &r,
                                 const vec3<T> &outward_normal) {
    front_face = dot(r.direction(), outward_normal) < 0;
    normal = front_face ? outward_normal : -outward_normal;
  }
};

template <class T> requires std::floating_point<T> class hittable {
public:
  constexpr hittable() = default;
  virtual constexpr ~hittable() = default;
  constexpr hittable(const hittable &) = default;
  constexpr auto operator=(const hittable &) -> hittable & = default;
  constexpr hittable(hittable &&) noexcept = default;
  constexpr auto operator=(hittable &&) noexcept -> hittable & = default;

  virtual constexpr auto hit(const ray<T> &r, T t_min, T t_max,
                             hit_record<T> &rec) const -> bool = 0;

  virtual constexpr auto bounding_box(T time0, T time1,
                                      aabb<T> &output_box) const -> bool = 0;

  [[nodiscard]] virtual auto pdf_value(const point3<T> & /*o*/,
                                       const vec3<T> & /*v*/) const -> T {
    return 0.0;
  }

  [[nodiscard]] virtual auto random(const vec3<T> & /*o*/) const -> vec3<T> {
    return vec3<T>(1, 0, 0);
  }
};

template <class T>
requires std::floating_point<T> class translate : public hittable<T> {
public:
  translate(std::shared_ptr<hittable<T>> p, const vec3<T> &displacement)
      : ptr(std::move(p)), offset(displacement) {}

  auto hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
      -> bool override {
    ray moved_r(r.origin() - offset, r.direction(), r.time());
    if (!ptr->hit(moved_r, t_min, t_max, rec)) {
      return false;
    }

    rec.p += offset;
    rec.set_face_normal(moved_r, rec.normal);

    return true;
  }

  auto bounding_box(T time0, T time1, aabb<T> &output_box) const
      -> bool override {
    if (!ptr->bounding_box(time0, time1, output_box)) {
      return false;
    }

    output_box = aabb(output_box.min() + offset, output_box.max() + offset);

    return true;
  }

private:
  std::shared_ptr<hittable<T>> ptr;
  vec3<T> offset;
};

template <class T>
requires std::floating_point<T> class rotate_y : public hittable<T> {
public:
  rotate_y(std::shared_ptr<hittable<T>> p, T angle);

  auto hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
      -> bool override;

  auto bounding_box(T /*time0*/, T /*time1*/, aabb<T> &output_box) const
      -> bool override {
    output_box = bbox;
    return hasbox;
  }

private:
  std::shared_ptr<hittable<T>> ptr;
  T sin_theta;
  T cos_theta;
  bool hasbox;
  aabb<T> bbox;
};

template <class T>
requires std::floating_point<T>
rotate_y<T>::rotate_y(std::shared_ptr<hittable<T>> p, T angle)
    : ptr(std::move(p)) {
  auto radians = degrees_to_radians(angle);
  sin_theta = std::sin(radians);
  cos_theta = std::cos(radians);
  hasbox = ptr->bounding_box(0, 1, bbox);

  point3<T> min(infinity<T>, infinity<T>, infinity<T>);
  point3<T> max(-infinity<T>, -infinity<T>, -infinity<T>);

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 2; j++) {
      for (int k = 0; k < 2; k++) {
        auto x = i * bbox.max().x() + (1 - i) * bbox.min().x();
        auto y = j * bbox.max().y() + (1 - j) * bbox.min().y();
        auto z = k * bbox.max().z() + (1 - k) * bbox.min().z();

        auto newx = cos_theta * x + sin_theta * z;
        auto newz = -sin_theta * x + cos_theta * z;

        vec3<T> tester(newx, y, newz);

        for (int c = 0; c < 3; c++) {
          min[c] = fmin(min[c], tester[c]);
          max[c] = fmax(max[c], tester[c]);
        }
      }
    }
  }

  bbox = aabb(min, max);
}

template <class T>
requires std::floating_point<T> auto
rotate_y<T>::hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
    -> bool {
  auto origin = r.origin();
  auto direction = r.direction();

  origin[0] = cos_theta * r.origin()[0] - sin_theta * r.origin()[2];
  origin[2] = sin_theta * r.origin()[0] + cos_theta * r.origin()[2];

  direction[0] = cos_theta * r.direction()[0] - sin_theta * r.direction()[2];
  direction[2] = sin_theta * r.direction()[0] + cos_theta * r.direction()[2];

  ray rotated_r(origin, direction, r.time());

  if (!ptr->hit(rotated_r, t_min, t_max, rec)) {
    return false;
  }

  auto p = rec.p;
  auto normal = rec.normal;

  p[0] = cos_theta * rec.p[0] + sin_theta * rec.p[2];
  p[2] = -sin_theta * rec.p[0] + cos_theta * rec.p[2];

  normal[0] = cos_theta * rec.normal[0] + sin_theta * rec.normal[2];
  normal[2] = -sin_theta * rec.normal[0] + cos_theta * rec.normal[2];

  rec.p = p;
  rec.set_face_normal(rotated_r, normal);

  return true;
}

template <class T>
requires std::floating_point<T> class flip_face : public hittable<T> {
public:
  explicit flip_face(std::shared_ptr<hittable<T>> p) : ptr(std::move(p)) {}

  auto hit(const ray<T> &r, T t_min, T t_max, hit_record<T> &rec) const
      -> bool override {

    if (!ptr->hit(r, t_min, t_max, rec)) {
      return false;
    }

    rec.front_face = !rec.front_face;
    return true;
  }

  auto bounding_box(T time0, T time1, aabb<T> &output_box) const
      -> bool override {
    return ptr->bounding_box(time0, time1, output_box);
  }

private:
  std::shared_ptr<hittable<T>> ptr;
};

#endif
