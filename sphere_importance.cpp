#include <concepts>
#include <iomanip>
#include <iostream>

#include "convenience.hpp"
#include "vec3.hpp"

template <class T>
requires std::floating_point<T> constexpr auto pdf(const vec3<T> & /*x*/) -> T {
  return static_cast<T>(1 / (4 * pi<T>));
}

auto main() -> int {
  using Tfloat = double;
  using uint = unsigned int;
  constexpr uint N = 1000000;
  constexpr uint num_digits = 12;
  std::cout << std::fixed << std::setprecision(num_digits);

  auto sum = 0.0;
  for (uint i = 0; i < N; i++) {
    const auto d = random_unit_vector<Tfloat>();
    const auto cosine_squared = d.z() * d.z();
    sum += cosine_squared / pdf(d);
  }
  std::cout << "I = " << sum / N << '\n';
}

