
#include <cmath>
#include <iomanip>
#include <iostream>

#include "convenience.hpp"

auto main() -> int {
  using Tfloat = double;
  using uint = unsigned int;
  constexpr uint sqrt_N = 10000;
  constexpr uint n_decimals = 12;
  std::cout << std::fixed << std::setprecision(n_decimals);

  uint inside_circle = 0;
  uint inside_circle_stratified = 0;
  for (uint i = 0; i < sqrt_N; i++) {
    for (uint j = 0; j < sqrt_N; j++) {
      auto x = random_between<Tfloat>(-1, 1);
      auto y = random_between<Tfloat>(-1, 1);
      if (x * x + y * y < 1) {
        inside_circle++;
      }
      x = 2 * ((i + random_real<Tfloat>()) / sqrt_N) - 1;
      y = 2 * ((j + random_real<Tfloat>()) / sqrt_N) - 1;
      if (x * x + y * y < 1) {
        inside_circle_stratified++;
      }
    }
  }

  auto N = static_cast<double>(sqrt_N) * sqrt_N;
  std::cout << "Regular    Estimate of Pi = "
            << 4 * static_cast<Tfloat>(inside_circle) / N << '\n'
            << "Stratified Estimate of Pi = "
            << 4 * static_cast<Tfloat>(inside_circle_stratified) / N << '\n';
}

