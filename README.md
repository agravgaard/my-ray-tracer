A spare-time project to learn about ray-tracing and maybe some C++20

 - [x] [_Ray Tracing in One Weekend_](https://raytracing.github.io/books/RayTracingInOneWeekend.html)
 - [x] [_Ray Tracing: The Next Week_](https://raytracing.github.io/books/RayTracingTheNextWeek.html)
 - [x] [_Ray Tracing: The Rest of Your Life_](https://raytracing.github.io/books/RayTracingTheRestOfYourLife.html)

## How to use:

Requires a C++ 20 compiler with OpenMP and meson
```
git clone https://gitlab.com/agravgaard/my-ray-tracer.git
cd my-ray-tracer
meson setup build
meson compile -C build
./build/raytracer > image.ppm
```

## Expected output at tag: "First-book" (converted to png):
![Balls on a surface with reflections a little depth of field](expected_out_book1.png)

## Expected output at tag: "Second-book" (converted to png):
![Floating balls and boxes of different textures](expected_out_book2.png)

## Expected output at tag: "Third-book" (converted to png):
![Cornell box with aluminium box and glass ball](expected_out_book3.png)
