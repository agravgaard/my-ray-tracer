#include <concepts>
#include <iomanip>
#include <iostream>

#include "convenience.hpp"

constexpr auto three = 3.0;
constexpr auto eight = 8.0;

template <class T>
    requires std::floating_point<T> ||
    std::integral<T> constexpr auto pdf(T x) -> T {
  return static_cast<T>(three * x * x / eight);
}

auto main() -> int {
  using Tfloat = double;
  using uint = unsigned int;
  constexpr uint N = 1000000;
  constexpr uint num_digits = 12;
  std::cout << std::fixed << std::setprecision(num_digits);

  auto sum = 0.0;
  for (uint i = 0; i < N; i++) {
    const auto x = std::pow(random_between<Tfloat>(0, eight), 1. / three);
    sum += x * x / pdf(x);
  }

  std::cout << "I = " << 2 * sum / N << '\n';
}
