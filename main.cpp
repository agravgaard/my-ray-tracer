#include <cmath>
#include <cstdlib>
#include <iostream>
#include <memory>

#include "aarect.hpp"
#include "camera.hpp"
#include "color.hpp"
#include "convenience.hpp"
#include "hittable.hpp"
#include "hittable_list.hpp"
#include "pdf.hpp"
#include "ray.hpp"
#include "texture.hpp"
#include "vec3.hpp"

#include "example_worlds.hpp"

#ifndef SPP
constexpr auto default_samples_per_pixel = 100;
#else
constexpr auto default_samples_per_pixel = SPP;
#endif

constexpr auto default_image_width = 400;
constexpr auto default_aspect_ratio = 16.0 / 9.0;
constexpr auto default_angle = 40.0;

using Tfloat = double;

constexpr auto hit_tol = 0.001;

template <class T>
requires std::floating_point<T> [[nodiscard]] auto
ray_color(const ray<T> &r, const color<T> &background,
          const hittable_list<T> &world,
          const std::shared_ptr<hittable_list<T>> &lights, int depth)
    -> color<T> {
  hit_record<T> rec;

  // If we've exceeded the ray bounce limit, no more light is gathered.
  if (depth <= 0) {
    return color<T>(0, 0, 0);
  }

  // If the ray hits nothing, return the background color.
  if (!world.hit(r, hit_tol, infinity<T>, rec)) {
    return background;
  }

  scatter_record<T> srec;
  auto emitted = rec.mat->emitted(r, rec, rec.u, rec.v, rec.p);
  if (!rec.mat->scatter(r, rec, srec)) {
    return emitted;
  }

  if (srec.is_specular) {
    return srec.attenuation *
           ray_color(srec.specular_ray, background, world, lights, depth - 1);
  }

  auto light_ptr = std::make_shared<hittable_pdf<T>>(lights, rec.p);
  mixture_pdf<T> p(light_ptr, srec.pdf_ptr);

  ray scattered = ray(rec.p, p.generate(), r.time());
  auto pdf_val = p.value(scattered.direction());

  return emitted +
         srec.attenuation * rec.mat->scattering_pdf(r, rec, scattered) *
             ray_color(scattered, background, world, lights, depth - 1) /
             pdf_val;
}

enum struct world_t {
  world_one,
  world_perlin,
  world_earth,
  rolling_stones,
  world_cornell,
  world_cornell_smoke,
  world_final_book2,
};

template <class T>
requires std::floating_point<T> constexpr auto get_lights(world_t w)
    -> std::shared_ptr<hittable_list<T>> {
  auto lights = std::make_shared<hittable_list<T>>();
  switch (w) {
  case world_t::world_one:
  case world_t::world_perlin:
  case world_t::world_earth:
  case world_t::rolling_stones:
  case world_t::world_final_book2:
  case world_t::world_cornell:
  case world_t::world_cornell_smoke:
  default:
    lights->add(std::make_shared<xz_rect<T>>(i213, i343, i227, i332, i554,
                                             std::shared_ptr<material<T>>()));

    lights->add(std::make_shared<sphere<T>>(point3<T>(i190, i90, i190), i90,
                                            std::shared_ptr<material<T>>()));
    break;
  }
  return lights;
}

template <class T>
requires std::floating_point<T> constexpr auto get_world(world_t w)
    -> hittable_list<T> {
  switch (w) {
  case world_t::world_one:
    return random_scene<T>();
  case world_t::world_perlin:
    return two_perlin_spheres<T>();
  case world_t::world_earth:
    return earth<T>();
  case world_t::rolling_stones:
    return simple_light<T>();
  case world_t::world_cornell:
    return cornell_box<T>();
  case world_t::world_cornell_smoke:
    return cornell_smoke<T>();
  case world_t::world_final_book2:
    return final_book2<T>();
  default:
    return two_spheres<T>();
  }
}

template <class T> requires std::floating_point<T> struct settings {
  T aspect_ratio = default_aspect_ratio;
  int image_width = default_image_width;
  int samples_per_pixel = default_samples_per_pixel;
  T vfov = default_angle;
  T aperture = 0.0;
  color<T> background{0, 0, 0};
  point3<T> lookfrom{thirteen, 2, 3};
  point3<T> lookat{0, 0, 0};
};

template <class T>
requires std::floating_point<T> constexpr auto get_settings(const world_t w)
    -> settings<T> {
  settings<T> s{};

  constexpr auto i278 = 278;
  constexpr auto i478 = 478;
  constexpr auto i800 = 800;
  constexpr auto i600 = 600;
  constexpr auto i10000 = 10000;

  switch (w) {
  case world_t::world_one:
    s.aperture = p1;
    s.vfov = 2 * ten;
    s.background = color<Tfloat>(p7, p8, 1.00);
    return s;
  case world_t::rolling_stones:
    s.samples_per_pixel = p4 * thousand;
    s.lookfrom = point3<Tfloat>(thirteen * 2, 3, p6 * ten);
    s.lookat = point3<Tfloat>(0, 2, 0);
    s.vfov = 2 * ten;
    return s;
  case world_t::world_cornell:
  case world_t::world_cornell_smoke:
    s.aspect_ratio = 1.0;
    s.image_width = i600;
    s.samples_per_pixel = default_samples_per_pixel;
    s.lookfrom = point3<T>(i278, i278, -i800);
    s.lookat = point3<T>(i278, i278, 0);
    return s;
  case world_t::world_final_book2:
    s.aspect_ratio = 1.0;
    s.image_width = i800;
    s.samples_per_pixel = i10000;
    s.lookfrom = point3<T>(i478, i278, -i600);
    s.lookat = point3<T>(i278, i278, 0);
    return s;
  case world_t::world_earth:
    s.lookfrom = point3<T>(0, 0, twelve);
  case world_t::world_perlin:
  default:
    s.background = color<Tfloat>(p7, p8, 1.00);
    s.vfov = 2 * ten;
  }
  return s;
}

auto main() -> int {
  constexpr auto w = world_t::world_cornell;
  constexpr auto s = get_settings<Tfloat>(w);
  const auto world = get_world<Tfloat>(w);
  const auto lights = get_lights<Tfloat>(w);

  // Camera
  constexpr int max_depth = 50;
  constexpr vec3<Tfloat> vup{0, 1, 0};
  constexpr auto dist_to_focus = 10.0;
  camera cam(s.lookfrom, s.lookat, vup, s.vfov, s.aspect_ratio, s.aperture,
             dist_to_focus, 0.0, 1.0);

  constexpr auto samples_per_pixel = s.samples_per_pixel;
  constexpr auto image_width = s.image_width;
  constexpr auto background = s.background;
  constexpr int image_height = static_cast<int>(s.image_width / s.aspect_ratio);

  // Render

  std::cout << "P3\n" << image_width << ' ' << image_height << "\n255\n";

  auto img = std::array<std::string, image_height>();

#pragma omp parallel for shared(cam, image_height, world, lights,              \
                                samples_per_pixel, img, background,            \
                                max_depth) default(none)
  for (int j = image_height - 1; j >= 0; --j) {
    std::string line;
    for (int i = 0; i < image_width; ++i) {
      color<Tfloat> pixel_color(0, 0, 0);
      for (int s = 0; s < samples_per_pixel; ++s) {
        auto u = (i + random_real<Tfloat>()) / (image_width - 1);
        auto v = (j + random_real<Tfloat>()) / (image_height - 1);
        ray r = cam.get_ray(u, v);
        pixel_color += ray_color(r, background, world, lights, max_depth);
      }
      line += write_color(pixel_color, samples_per_pixel);
    }
    img.at(image_height - 1 - j) = line;
  }
  write_image<image_height>(std::cout, img);

  return 0;
}
